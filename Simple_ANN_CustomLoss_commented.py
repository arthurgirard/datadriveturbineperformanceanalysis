# -*- coding: utf-8 -*-
"""
@author: arthur girard - agirard@ethz.ch
"""
# imports several libraries that will be used throughout the script.
# The libraries are Pandas for data manipulation, 
# Numpy for numerical operations, Matplotlib for data visualization, 
# TensorFlow and Keras for building neural networks.
import pandas as pd
import glob
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
import math
import numpy as np
import pandas as pd
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from sklearn.preprocessing import StandardScaler
import tensorflow.keras.backend as K

# modify the number below accordingly
#Participant ID number
id_number = 5 
#Submission Number
Submission_number = 1

# Assuming that you are running this code in CurrentFolder
# Your folder should be structured as CurrentFolder\Dataset\Kelmarsh_training_data\Kel_df1_training.csv

#reads a CSV file from the given file path and returns a Pandas DataFrame 
def generate_dataframe(filepath):
  dataframe = pd.read_csv(filepath)
  return dataframe

# load only 1 .csv file
#filepath = 'Dataset/Kelmarsh_training_data/Kel_df1_training.csv'
#dataframe_training = generate_dataframe(filepath)
#print(dataframe_training.shape)

# load all the .csv file in the folder that end with "training"
# define the file paths of the CSV files 
path_training = r'Dataset\Kelmarsh_training_data\*training.csv'
path2 = r'Dataset\Penmanshiel_training_data\*training.csv'
files = glob.glob(path_training)+ glob.glob(path2)
#files =  glob.glob(path2)

# Create an empty pandas dataframe to store the training data
dataframe_training = pd.DataFrame()

for file in files:
    dataframe_training = generate_dataframe(file)
    Current_CSV_file = file.split("\\")[2]
    print('Shape of the dataframe training is :',dataframe_training.shape)    

    # Convert 'time' column to datetime object using the strftime format
    dataframe_training['time'] = pd.to_datetime(dataframe_training['time'], format='%m/%d/%Y %H:%M')

    # Create a new column 'month'
    dataframe_training['month'] = dataframe_training['time'].dt.month_name()

    #Create a new column 'day or night'
    #Considering that the day is between 6am and 6pm 
    dataframe_training['Day.Night'] = dataframe_training['time'].apply(lambda x: 'Day' if x.hour > 6 and x.hour < 18 else 'Night')

    # replace the occurence 'Day'/'nigh' by numeric value '1'/'0' 
    dataframe_training = dataframe_training.replace({"Day": 1, "Night": 0})

    #create a mapping to replace the occurence of the month in the dataframe:
    month_mapping = {'January': 1, 'February': 2, 'March': 3, 'April': 4, 'May': 5, 'June': 6,
                  'July': 7, 'August': 8, 'September': 9, 'October': 10, 'November': 11, 'December': 12}
    dataframe_training = dataframe_training.replace(month_mapping)
    dataframe_training=dataframe_training.drop(columns='time', axis=1)

    from sklearn.model_selection import train_test_split

    #Define the attributes that are characteristics of the input/output
#   column_list_input = ['wind_speed', 'wind_speed_sensor1', 'wind_speed_sensor1_SD', 'wind_speed_sensor2', 'wind_speed_sensor2_SD', 'density_adjusted_wind_speed', 'wind_direction', 'nacelle_position', 'wind_direction_SD', 'nacelle_position_SD', 'nacelle_ambient_temperature', 'TI', 'month','Day.Night']
    column_list_input = ['wind_speed', 'wind_speed_sensor1', 'wind_speed_sensor1_SD', 'wind_speed_sensor2', 'wind_speed_sensor2_SD', 'density_adjusted_wind_speed', 'wind_direction', 'nacelle_position', 'wind_direction_SD', 'nacelle_position_SD', 'nacelle_ambient_temperature', 'TI','Day.Night']

    #column_list_input = ['wind_speed']
    column_list_ouput = ['power']
    
    # Split the dataframe into training and testing sets
    train_dataframe, test_dataframe = train_test_split(dataframe_training, test_size=0.2, random_state=42)
    
    # Extract the input and output features from the training and testing dataframes
    x_train = train_dataframe[column_list_input]
    y_train = train_dataframe[column_list_ouput]
    x_train.fillna(0)
    y_train.fillna(0)
    x_train = x_train.values
    y_train = y_train.values
    
    #print(x_train)
    
    x_test = test_dataframe[column_list_input]
    y_test = test_dataframe[column_list_ouput]
    x_test.fillna(0)
    y_test.fillna(0)
    
    y_test =y_test.values
    x_test =x_test.values


    # Standardize a dataframe
    # Initialize StandardScaler
    scaler = StandardScaler()

    # Define custom loss function that computes both root mean squared error and mean absolute error    
    def custom_loss(y_true, y_pred):
        rmse = K.sqrt(K.mean(K.square(y_pred - y_true)))
        mae = K.mean(K.abs(y_pred - y_true))
        return (rmse + mae)/ 2

    # Build the neural network using the specific custom loss function defined above
    def build_model_custom_loss(input_shape):
        model = tf.keras.Sequential()
        model.add(tf.keras.layers.Dense(32, activation='sigmoid', input_shape=(input_shape,)))
        model.add(tf.keras.layers.Dense(32, activation='sigmoid'))
        model.add(tf.keras.layers.Dense(32, activation='sigmoid'))
        model.add(tf.keras.layers.Dense(1, activation='sigmoid'))
        optimizer = tf.keras.optimizers.Adam(clipnorm=1, learning_rate=0.01)
        model.compile(optimizer=optimizer, loss=custom_loss, metrics=['mean_squared_error'])
        return model
    
    # Train the neural network
    def train_model(model, df_train, target_train, epochs=40):
        model.fit(df_train, target_train, epochs=epochs, verbose=True)
    
    # Predict on the test set
    def predict(model, df_test):
        return model.predict(df_test)
    
    x_train = scaler.fit_transform(x_train)
    x_test = scaler.transform(x_test)
    
    # Build and train the model
    model = build_model_custom_loss(x_train.shape[1])
    train_model(model, x_train, y_train)
    
    # Print a summary of the model
    model.summary()
    
    # Evaluate the model using test set
    loss, accuracy = model.evaluate(x_test, y_test, verbose=False)
    print("Testing Accuracy: {:.4f}".format(accuracy))
    
    # Predict on the test set
    predictions = predict(model, x_test)
    
    x_test = scaler.inverse_transform(x_test)
    x_test_plot  = x_test[:, 0]
    
    def rmse(y_true, y_pred):
        return np.sqrt(np.mean((y_true - y_pred) ** 2))
    
    rmse = rmse(y_test, predictions)
    print("rmse: {:.4f}".format(rmse))
    
    plt.scatter(x_test_plot, y_test,color="blue", label="ground truth",s=0.1)
    plt.scatter(x_test_plot, predictions, color="red", label="Prediction on test set using ANN",s=0.1)
    title_with_nameFile = "Outpout power for " + str(Current_CSV_file)
    plt.title(title_with_nameFile)
    plt.xlabel("Wind speed (m/s)")
    plt.ylabel("Output power (normalized)")
    plt.legend()
    plt.show()

    # Loading the real test data: 
    import csv
    dataframe_test = pd.DataFrame()
    new_file_name = file.replace("training", "test")
    dataframe_test = generate_dataframe(new_file_name)
    dataframe_test = dataframe_test.replace({"Day": 1, "Night": 0})
    #create a mapping to replace the occurence of the month in the dataframe:
    month_mapping = {'January': 1, 'February': 2, 'March': 3, 'April': 4, 'May': 5, 'June': 6,
                      'July': 7, 'August': 8, 'September': 9, 'October': 10, 'November': 11, 'December': 12}
    dataframe_test = dataframe_test.replace(month_mapping)
    #remove the first 2 columns 
    dataframe_test = dataframe_test.iloc[:, 2:]
    dataframe_test = dataframe_test[column_list_input]
    # get the prediction
    dataframe_test = scaler.transform(dataframe_test)
    prediction = predict(model, dataframe_test)
    # load the csv file into a pandas DataFrame
    df_test = pd.read_csv(new_file_name)
    # create a numpy array of float32 of shape [None, 1]
    # fill the second column with the prediction array
    df_test.iloc[:, 1] = prediction
    # save the DataFrame as a new csv file
    directory_Name  = new_file_name.split("\\")[0]
    Subdirectory_Name = new_file_name.split("\\")[1]
    csv_name = new_file_name.split("\\")[2]
    NewNamePrefix = str(id_number) + '_#' + str(Submission_number) + '_'
    new_file_name3 = directory_Name + '\\' + Subdirectory_Name + '\\'+ NewNamePrefix + csv_name
    df_test.to_csv(new_file_name3, index=False)
    
    

