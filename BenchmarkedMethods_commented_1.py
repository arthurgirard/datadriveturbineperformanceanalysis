# -*- coding: utf-8 -*-
"""
@author: arthur girard - agirard@ethz.ch
"""
# imports several libraries that will be used throughout the script.
# The libraries are Pandas for data manipulation, 
# Numpy for numerical operations, Matplotlib for data visualization, 
# TensorFlow and Keras for building neural networks.
import pandas as pd
import glob
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
import math
import numpy as np
import pandas as pd
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from sklearn.preprocessing import StandardScaler
import tensorflow.keras.backend as K

# modify the number below accordingly
#Participant ID number
#id_number = 5 
#Submission Number
#Submission_number = 1

# Assuming that you are running this code in CurrentFolder
# Your folder should be structured as CurrentFolder\Dataset\Kelmarsh_training_data\Kel_df1_training.csv

#reads a CSV file from the given file path and returns a Pandas DataFrame 
def generate_dataframe(filepath):
  dataframe = pd.read_csv(filepath)
  return dataframe

# Performance function
def rmse_f(y_true, y_pred):
    return np.sqrt(np.mean((y_true - y_pred) ** 2))
def mae_f(y_true, y_pred):
    return np.mean(np.abs(y_true - y_pred))
def mse_f(y_true, y_pred):
    return ((y_true - y_pred) ** 2).mean()
def explained_variance_score(y_true, y_pred):
    numerator = ((y_true - y_pred) ** 2).sum()
    denominator = ((y_true - y_true.mean()) ** 2).sum()
    evs = 1 - (numerator / denominator)
    return evs

# load all the .csv file in the folder that end with "training"
# define the file paths of the CSV files 

#D:\Dropbox\2000_WedoWind\Dataset_split_noTime

path_training = r'D:\Dropbox\2000_WedoWind\Dataset_split_noTime\*x_train.csv'
#path2 = r'Dataset\Penmanshiel_training_data\*training.csv'
files = glob.glob(path_training)
#files =  glob.glob(path2)

# Create an empty pandas dataframe to store the training data
dataframe_training = pd.DataFrame()

# Create an dataframe to store the performance of each method and for each turbine
df_performance = pd.DataFrame(columns=['Turbine_ID', 'method', 'RMSE', 'MAE', 'MSE', 'EVS'])


for file in files:
    # Find the start and end index of the "Kel_df1" substring
    start_index = file.find("Kel_")
    end_index = start_index + len("Kel_df1")

    # Extract the wind turbin name 
    Turbine_id = file[start_index:end_index]

    # Load the training and test dataset
    x_train = np.genfromtxt(file, delimiter=',')
    file_x_test = file.replace("x_train", "x_test")
    x_test = np.genfromtxt(file, delimiter=',')
    file_y_train = file.replace("x_train", "y_train")
    y_train = np.genfromtxt(file_y_train, delimiter=',')
    file_y_test = file.replace("x_train", "y_test")
    y_test = np.genfromtxt(file_y_train, delimiter=',')

    # Standardize a dataframe
    # Initialize StandardScaler
    scaler = StandardScaler()
    x_train = scaler.fit_transform(x_train)
    x_test = scaler.transform(x_test)

    # Import SVMPowerCurve
    from dswe import SVMPowerCurve
    model = SVMPowerCurve()
    model.fit(x_train, y_train)
    predictions = model.predict(x_test)
    method = 'SVM'
    # Evaluate the model using test set

    x_test_plot = scaler.inverse_transform(x_test)
    x_test_plot  = x_test_plot[:, 0]
    rmse = rmse_f(y_test, predictions)
    mae = mae_f(y_test, predictions)
    mse = mse_f(y_test, predictions)
    evs = explained_variance_score(y_test, predictions)
    
    plt.scatter(x_test_plot, y_test,color="blue", label="ground truth",s=0.1)
    plt.scatter(x_test_plot, predictions, color="red", label="Prediction on test set using SVM",s=0.1)
    title_with_nameFile = "Outpout power for " + str(Turbine_id)
    plt.title(title_with_nameFile)
    plt.xlabel("Wind speed (m/s)")
    plt.ylabel("Output power (normalized)")
    plt.legend()
    plt.show()
    
    df_performance = df_performance.append({'Turbine_ID': Turbine_id, 'method': method, 'RMSE': rmse, 'MAE': mae, 'MSE': mse, 'EVS': evs}, ignore_index=True)

#### - New benchmarkMethod 

    # Import KNNPowerCurve
    from dswe import KNNPowerCurve
    model = KNNPowerCurve()
    model.fit(x_train, y_train)
    predictions = model.predict(x_test)
    method = 'KNN'
    x_test_plot = scaler.inverse_transform(x_test)
    x_test_plot  = x_test_plot[:, 0]
    
    rmse = rmse_f(y_test, predictions)
    mae = mae_f(y_test, predictions)
    mse = mse_f(y_test, predictions)
    evs = explained_variance_score(y_test, predictions)
    
    plt.scatter(x_test_plot, y_test,color="blue", label="ground truth",s=0.1)
    plt.scatter(x_test_plot, predictions, color="red", label="Prediction on test set using KNN",s=0.1)
    title_with_nameFile = "Outpout power for " + str(Turbine_id)
    plt.title(title_with_nameFile)
    plt.xlabel("Wind speed (m/s)")
    plt.ylabel("Output power (normalized)")
    plt.legend()
    plt.show()
    
    df_performance = df_performance.append({'Turbine_ID': Turbine_id, 'method': method, 'RMSE': rmse, 'MAE': mae, 'MSE': mse, 'EVS': evs}, ignore_index=True)

    # Import DNNPowerCurve
    from dswe import DNNPowerCurve
    dnn = DNNPowerCurve(train_all=True, save_fig=False)
    dnn.train(x_train, y_train)
    
    # Predict on the test set
    method = 'DNN'
    predictions = dnn.predict(x_test)

    x_test_plot = scaler.inverse_transform(x_test)
    x_test_plot  = x_test_plot[:, 0]
    
    
    rmse = rmse_f(y_test, predictions)
    mae = mae_f(y_test, predictions)
    mse = mse_f(y_test, predictions)
    evs = explained_variance_score(y_test, predictions)
    
    
    plt.scatter(x_test_plot, y_test,color="blue", label="ground truth",s=0.1)
    plt.scatter(x_test_plot, predictions, color="red", label="Prediction on test set using DNN",s=0.1)
    title_with_nameFile = "Outpout power for " + str(Turbine_id)
    plt.title(title_with_nameFile)
    plt.xlabel("Wind speed (m/s)")
    plt.ylabel("Output power (normalized)")
    plt.legend()
    plt.show()
    
    df_performance = df_performance.append({'Turbine_ID': Turbine_id, 'method': method, 'RMSE': rmse, 'MAE': mae, 'MSE': mse, 'EVS': evs}, ignore_index=True)


import matplotlib.pyplot as plt
import seaborn as sns

# Initialize the figure
f, ax = plt.subplots()
sns.pointplot(
    data=df_performance, x="RMSE", y="Turbine_ID", hue="method", dodge=True
)
plt.tight_layout()
# Show the plot
plt.show()

# Initialize the figure
f, ax = plt.subplots()
sns.pointplot(
    data=df_performance, x="MAE", y="Turbine_ID", hue="method", dodge=True
)
plt.tight_layout()
# Show the plot
plt.show()

# Initialize the figure
f, ax = plt.subplots()
sns.pointplot(
    data=df_performance, x="MSE", y="Turbine_ID", hue="method", dodge=True
)
plt.tight_layout()
# Show the plot
plt.show()

# Initialize the figure
f, ax = plt.subplots()
sns.pointplot(
    data=df_performance, x="EVS", y="Turbine_ID", hue="method", dodge=True
)
plt.tight_layout()
# Show the plot
plt.show()


# Create a grid of subplots with 2 rows and 2 columns
fig, axs = plt.subplots(nrows=2, ncols=2, figsize=(8, 8), sharey=True, sharex=False)

# Plot the RMSE subplot
sns.pointplot(
    data=df_performance, x="RMSE", y="Turbine_ID", hue="method", dodge=True, ax=axs[0, 0]
)
axs[0, 0].set_ylabel('Turbine_ID', fontsize=12)
axs[0, 0].set_xlabel('RMSE', fontsize=12)

# Plot the MAE subplot
sns.pointplot(
    data=df_performance, x="MAE", y="Turbine_ID", hue="method", dodge=True, ax=axs[0, 1]
)
axs[0, 1].set_ylabel('')
axs[0, 1].set_xlabel('MAE', fontsize=12)
#axs[0, 1].set_yticklabels([])

# Plot the MSE subplot
sns.pointplot(
    data=df_performance, x="MSE", y="Turbine_ID", hue="method", dodge=True, ax=axs[1, 0]
)
axs[1, 0].set_ylabel('Turbine_ID', fontsize=12)
axs[1, 0].set_xlabel('MSE', fontsize=12)

# Plot the EVS subplot
sns.pointplot(
    data=df_performance, x="EVS", y="Turbine_ID", hue="method", dodge=True, ax=axs[1, 1]
)
axs[1, 1].set_ylabel('')
axs[1, 1].set_xlabel('EVS', fontsize=12)
#axs[1, 1].set_yticklabels([])

# Improve the layout of the subplots
plt.tight_layout()

# Show the plot
plt.show()


# Create a grid of subplots with 2 rows and 2 columns
fig, axs = plt.subplots(nrows=2, ncols=2, figsize=(8, 8), sharey=True, sharex=False)

# Plot the RMSE subplot
sns.pointplot(
    data=df_performance, x="RMSE", y="Turbine_ID", hue="method", dodge=True, ax=axs[0, 0]
)
axs[0, 0].set_ylabel('Turbine_ID', fontsize=16)
axs[0, 0].set_xlabel('RMSE', fontsize=16)

# Plot the MAE subplot
sns.pointplot(
    data=df_performance, x="MAE", y="Turbine_ID", hue="method", dodge=True, ax=axs[0, 1]
)
axs[0, 1].set_ylabel('')
axs[0, 1].set_xlabel('MAE', fontsize=16)
#axs[0, 1].set_yticklabels([])

# Plot the MSE subplot
sns.pointplot(
    data=df_performance, x="MSE", y="Turbine_ID", hue="method", dodge=True, ax=axs[1, 0]
)
axs[1, 0].set_ylabel('Turbine_ID', fontsize=16)
axs[1, 0].set_xlabel('MSE', fontsize=16)

# Plot the EVS subplot
sns.pointplot(
    data=df_performance, x="EVS", y="Turbine_ID", hue="method", dodge=True, ax=axs[1, 1]
)
axs[1, 1].set_ylabel('')
axs[1, 1].set_xlabel('EVS', fontsize=16)
#axs[1, 1].set_yticklabels([])

# Add legend outside the subplots
handles, labels = axs[0, 0].get_legend_handles_labels()
fig.legend(handles, labels, bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)

# Improve the layout of the subplots
plt.tight_layout()

# Show the plot
plt.show()






    
    

